import React, { useState, useContext, useEffect } from "react";
import { Context } from "./context";
import axios from "axios";
import "../css/style.css";

const Movie = () => {
  const [movie, setMovie] = useContext(Context);
  const [input, setInput] = useState({
    title: "",
    description: "",
    duration: 0,
    genre: "",
    year: 0,
    rating: 0,
  });

  const [statusForm, setStatusForm] = useState("create");
  const [selectId, setSelectId] = useState(0);

  
  console.log(movie);
  useEffect(() => {
    if (movie.movieData === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/movies`)
        .then((res) => {
          let data = res.data.map((x) => {
            return {
              id: x.id,
              title: x.title,
              description: x.description,
              year: x.year,
              duration: x.duration,
              genre: x.genre,
              rating: x.rating,
            };
          });

          data.sort((a, b) => Number(b.rating) - Number(a.rating));
          setMovie({ ...movie, movieData: data });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [setMovie]);

  const handleEdit = (event) => {
    let idMovie = parseInt(event.target.value);
    let selectMovie = movie.movieData.find((el) => el.id === idMovie);
    setSelectId(idMovie);
    setStatusForm("edit");
    setInput({
      title: selectMovie.title,
      description: selectMovie.description,
      duration: selectMovie.duration,
      genre: selectMovie.genre,
      year: selectMovie.year,
      rating: selectMovie.rating,
    });
  };

  const handleDelete = (event) => {
    let idMovie = parseInt(event.target.value);

    let newLists = movie.movieData.filter((el) => el.id !== idMovie);

    axios
      .delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
      .then((res) => {
        console.log(res);
      });
    setMovie({ ...movie, movieData: [...newLists] });
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setInput((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleValidation = () => {
    let error = "";

    //validation title
    if (
      input["title"].replace(/\s/g, "") === "" ||
      typeof input["title"] !== "string"
    ) {
      error += `Title : Error data type or can't be empty\n`;
    }

    //validation description
    if (input["description"].replace(/\s/g, "") === "") {
      error += `Description : Error cant be empty \n`;
    }

    //validation year
    input["year"] = Number(input["year"]);
    if (input["year"] === 0 || typeof input["year"] !== "number") {
      error += `Year : Error data type or can't be empty \n`;
    }

    //validation duration
    input["duration"] = Number(input["duration"]);
    if (input["duration"] === 0 || typeof input["duration"] !== "number") {
      error += `Duration : Error data type or can't be empty \n`;
    }

    //validation genre
    if (
      input["genre"].replace(/\s/g, "") === "" ||
      typeof input["genre"] !== "string"
    ) {
      error += `Genre : Error data type or can't be empty \n`;
    }

    //validation rating
    input["rating"] = Number(input["rating"]);
    if (
      input["rating"] === 0 ||
      typeof input["rating"] !== "number" ||
      input["rating"] < 0 ||
      input["rating"] > 10
    ) {
      error += `Rating : Error data type (1-10) or can't be empty \n`;
    }

    return error;
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    let title = input.title
    let description = input.description

    console.log(movie.movieData);
    if(title.replace(/\s/g,'') !== "" && description.replace(/\s/g,'') !== ""){
    //handleValidation();
    if (statusForm === "create") {
      axios
        .post(`http://backendexample.sanbercloud.com/api/movies`, input)
        .then((res) => {
          setMovie({
            ...movie,
            movieData: [
              ...movie.movieData,
              {
                id: res.data.id,
                title: res.data.title,
                description: res.data.description,
                duration: res.data.duration,
                genre: res.data.genre,
                year: res.data.year,
                rating: res.data.rating,
              },
            ],
          });

          console.log(movie);
        });
    } else if (statusForm === "edit") {
      console.log(input);
      axios
        .put(
          `http://backendexample.sanbercloud.com/api/movies/${selectId}`,
          input
        )
        .then((res) => {
          console.log(res.data);
          let selectMovie = movie.movieData.find((el) => el.id === selectId);
          selectMovie.id = selectId;
          selectMovie.title = input.title;
          selectMovie.description = input.description;
          selectMovie.duration = input.duration;
          selectMovie.genre = input.genre;
          selectMovie.year = input.year;
          selectMovie.rating = input.rating;
          setMovie({
            ...movie,
            movieData: [...movie.movieData],
          });

          console.log(movie);
        });
    }
    setStatusForm("create");
    setSelectId(0);
    setInput({
      title: "",
      description: "",
      year: 0,
      duration: 0,
      genre: "",
      rating: 0,
    })
    }
  };

  return (
    <>
      <h1>Form Review Film Baru</h1>

      <div style={{ width: "50%", margin: "0 auto", display: "block" }}>
        <div style={{ border: "1px solid #aaa", padding: "20px" }}>
          <form onSubmit={handleSubmit}>
            <label style={{ float: "left" }}>Title:</label>
            <input
              style={{ float: "right" }}
              type="text"
              name="title"
              value={input.title}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Description:</label>
            <input
              style={{ float: "right" }}
              type="text"
              name="description"
              value={input.description}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Duration:</label>
            <input
              style={{ float: "right" }}
              type="number"
              name="duration"
              value={input.duration}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Genre:</label>
            <input
              style={{ float: "right" }}
              type="text"
              name="genre"
              value={input.genre}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Year:</label>
            <input
              style={{ float: "right" }}
              type="number"
              name="year"
              value={input.year}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Rating:</label>
            <input
              style={{ float: "right" }}
              type="number"
              name="rating"
              value={input.rating}
              onChange={handleChange}
            />
            <br />
            <br />
            <div style={{ width: "100%", paddingBottom: "20px" }}>
              <button style={{ float: "right" }} >submit</button>
            </div>
          </form>
        </div>
      </div>
      <h1>Movie Review List</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Duration</th>
            <th>Genre</th>
            <th>Year</th>
            <th>Rating</th>
            <th>Edit/Delete</th>
          </tr>
        </thead>
        <tbody>
          {movie.movieData !== null &&
            movie.movieData.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.title}</td>
                  <td>{item.description}</td>
                  <td>{item.duration}</td>
                  <td>{item.genre}</td>
                  <td>{item.year}</td>
                  <td>{item.rating}</td>
                  <td style={{ display: "block", margin: "auto" }}>
                    <button type="button" onClick={handleEdit} value={item.id}>
                      Edit
                    </button>
                    &nbsp;
                    <button
                      type="button"
                      onClick={handleDelete}
                      value={item.id}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </>
  );
};

export default Movie;
