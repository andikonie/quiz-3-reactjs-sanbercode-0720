import React, { useContext } from "react";
import logo from "../img/logo.png";
import { Link } from "react-router-dom";
import { Context } from "./context";
import {useHistory} from 'react-router-dom'

const Menu = () => {
  const [movie, setMovie] = useContext(Context);
  const history = useHistory();
  
  const handleLogout = () => {
        setMovie({
            ...movie,
            isLogin: false,
            loginUser: ""
            
        })
       
    history.push("/")
  }


  return (
    <header>
      <img className="logo" src={logo} alt="logo"></img>
      <nav>
        <ul>
          <li>
            <Link to="/">Home </Link>
          </li>
          <li>
            <Link to="/about">About </Link>
          </li>
          {movie.isLogin ? 
          <>
          <li>
            <Link to="/movies">Movie List Editor</Link> 
        </li> 
        <li>
           <button type="button" onClick={handleLogout} >Logout</button>
        </li>
        </>
          :
          <li>
            <Link to="/login">Login </Link>
          </li>
        }   
          
        </ul>
      </nav>
    </header>
  );
};

export default Menu;
