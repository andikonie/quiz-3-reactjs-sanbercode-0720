import React from "react"
import Menu from './menu'
import Routes from './routes'
import { Provider } from "./context"

const Index = () =>{
    return(
        <>
        <Provider>
        <Menu />
        <Routes />
        </Provider>
      </>
    )
  }
  
  export default Index;