import React, {useContext, useState, useEffect} from "react"
import {Context} from "./context"
import "../css/style.css";
import axios from "axios"; 


const Home = () => {
  const [movie, setMovie ] = useContext(Context);
  
      
  console.log(movie.isLogin);
  console.log(movie.loginUser);
  

   useEffect(()=>{
    if(movie.movieData === null){
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            
            let data = res.data.map(x => {
              return{
                id: x.id,
                title: x.title,
                description: x.description,
                year: x.year,
                duration: x.duration,
                genre: x.genre,
                rating: x.rating
              }
            })
            
            data.sort((a,b) => Number(b.rating) - Number(a.rating));
            setMovie({ ...movie, movieData: data });
            
            })
            .catch(err => {
                console.log(err);
          })
          
    }

  },[setMovie])

  
  return(
    <>
    <section>
      <h1>Movie Review List</h1>
      <div id="article-list">
        <div>
         {movie.movieData !== null && movie.movieData.map((item,index)=>{
           return(
            <div style={{marginBottom:"15px", marginLeft:"10px"}} key={index}>
            <div style={{marginLeft: "7px"}}>
                <h2 >{item.title}</h2>
                <p><strong>Rating: {item.rating}</strong></p>
                <p><strong>Duration: {Math.round(item.duration/60)} Hours</strong></p>
                <p><strong>Genre: {item.genre}</strong></p>
                <p><strong>Release Year: {item.year}</strong></p>
                <p>{item.description}</p>
            </div>
            </div>
           )
         })

         } 
        </div>
      </div>
    </section>
    <footer>
      <h5>copyright &copy; 2020 by Sanbercode</h5>
    </footer>
    </>
    )

}

export default Home