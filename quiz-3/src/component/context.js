import React, { useState, createContext } from "react";

export const Context = createContext();

export const Provider = props => {
  const [movie, setMovie] = useState({
    isLogin: false,
    loginUser: "",
    movieData: null
  })
  return (
    <Context.Provider value={[movie, setMovie]}>
      {props.children}
    </Context.Provider>
  );
};

