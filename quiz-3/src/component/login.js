import React, { useState, useContext } from "react";
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import {useHistory} from 'react-router-dom'
import {Context} from "./context";
import "./login.css";

export default function Login() {

  const [movie, setMovie ] = useContext(Context);
//   const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const history = useHistory();

  function validateForm() {
    return username.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    setMovie({
        isLogin: true,
        loginUser: username,
        movieData: movie.movieData
    })
    history.push("/")
  }

  

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="username" >
          <FormLabel >Username</FormLabel >
          <FormControl
            autoFocus
            type="username"
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" >
          <FormLabel >Password</FormLabel >
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <Button block  disabled={!validateForm()} type="submit">
          Login
        </Button>
      </form>
    </div>
  );
}
